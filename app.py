from flask import Flask, jsonify, request
import os

app = Flask(__name__)

sample_data = [
    {"IP": "127.0.0.1", "Hostname": "mta-prod-1", "Active": True},
    {"IP": "127.0.0.2", "Hostname": "mta-prod-1", "Active": False},
    {"IP": "127.0.0.3", "Hostname": "mta-prod-2", "Active": True},
    {"IP": "127.0.0.4", "Hostname": "mta-prod-2", "Active": True},
    {"IP": "127.0.0.5", "Hostname": "mta-prod-2", "Active": False},
    {"IP": "127.0.0.6", "Hostname": "mta-prod-3", "Active": False}
]

@app.route('/hostnames', methods=['GET'])
def get_hostnames():
    active_ip_threshold = int(os.getenv('ACTIVE_IP_THRESHOLD', '1'))

    filtered_hostnames = []
    for data in sample_data:
        hostname = data["Hostname"]
        active_ips = sum(1 for d in sample_data if d["Hostname"] == hostname and d["Active"])
        if active_ips <= active_ip_threshold and hostname not in filtered_hostnames:
            filtered_hostnames.append(hostname)

    response = {
        'hostnames': filtered_hostnames
    }
    return jsonify(response)

if __name__ == '__main__':
    app.run()
